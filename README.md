# Dead Spaniards at Gusen and Mauthausen concentration camps

Data liberated from spanish BOE-N-2019-544589 about dead Spaniards in Nazi concentration camps at Gusen and Mauthausen ["Anuncio de notificación de 6 de agosto de 2019 en procedimiento Listado de españolesfallecidos en los campos de concentración de Mauthausen y Gusen"](https://www.boe.es/boe/dias/2019/08/09/index.php?d=190).

## Data

The original listing was published in this [PDF](https://www.boe.es/boe_n/dias/2019/08/09/not.php?id=BOE-N-2019-544589). Using [Tabula](https://tabula.technology/) I managed to liberate the data table and convert it into that [CSV](https://gitlab.com/geraldo1/dead-spaniards-gusen-mauthausen/blob/master/_pdfs_BOE-N-2019-73da919ef9bfeb301fe0531d00e89b2110fe60ea.csv).

## Infovis

Addionally I made a choripleth map using [QGIS 3.4](https://gitlab.com/geraldo1/dead-spaniards-gusen-mauthausen/tree/master/qgis) showing deads by provinces where they have been born.

## Additional data:
* Spanish provinces: https://github.com/martgnz/es-atlas
* Marrocean border: https://africaopendata.org/dataset/morocco-maps